from math import hypot


def menu():

    print('\n1 - Расстояние между прямоугольниками по 4 сторонам\n'
        '2 - Расстояние между углами прямоугольников по 2 координатам')


def distance():    
    try:    
        a,b,c,d=map(int,input('\nВведите 4 числа через пробел для первого прямоугольника\n'
        '>  ').split())

        x,y,z,f=map(int,input('\nВведите 4 числа через пробел для второго прямоугольника\n'
        '>  ').split())
    
    except ValueError:
        print('\nНекорректный ввод, попробуйте снова')

    else:   
        rect1 = {'a':a, 'b':b, 'c':c, 'd':d}

        rect2 = {'x':x, 'y':y, 'z':z, 'f':f}

        res = min(rect1['a'] + rect1['c'] - rect2['x'], rect2['x'] + rect2['z']
        - rect1['a'])

        print(f'\nРасстояние между прямоугольниками = {res}\n')
        print()

def lenght():

    try:
        x1,y1=map(int,input('\nВведите числа через пробел для координат двух точек\n'
                '>  ').split())
        x2,y2=map(int,input('\nВведите числа через пробел для координат двух точек\n'
                '>  ').split())

    except ValueError:
        print('\nНекорректный ввод, поробуйте снова')
    
    else:    
        result = hypot(x2-x1, y2-y1)

        print(f'Результат = {result}')


def main():
    
    while True:
        
        menu()

        start = input('\nВыберете тип операции:  ')

        if start ==  '1':
            distance()
        
        elif start == '2':
            lenght()
        
        else:
            print('\nНекорректный ввод, поробуйте снова')
    
        cmd = input("\nНачать заново? (y / n) ")
        
        if cmd == 'n':
            break
        
        elif cmd == 'y':
            print()
        
        else:
            print('\nНекорректный ввод, попробуйте снова')
            
            
if __name__ == '__main__':
    main()